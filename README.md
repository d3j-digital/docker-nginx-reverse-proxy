# Docker NGINX Reverse Proxy
This project allows you to set up an NGINX reverse proxy inside a docker container so that you can proxy
local development domains to docker containers running on different ports. This is useful if you have
multiple development projects running in separate containers but would like to use easy to remember
hostnames to connect to them.

### Installation
From the root of this project run:
```shell
docker-compose up -d
```
The contents of the <b>vhosts</b> directory will be mounted into the container at <b>/etc/nginx/conf.d</b>

### Adding Virtual Hosts

#### Step 1: Create Host Config File
You can add more virtual host config files into the <b>vhosts</b> directory and they will automatically be
synced into the container, we have included an example config file within this repo.

#### Step 2: Connect to the Container
```shell
docker exec -it nginx-reverse-proxy /bin/sh
```
#### Step 3: Reload NGINX
```shell
nginx -s reload
```